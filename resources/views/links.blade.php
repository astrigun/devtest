  @extends('layouts.main')

  @section('content')

  <section class="container margin-50px-top">

    <div class="row">
      <div class="col-md-12 text-center">  
        <form id="formurl" role="form" method="POST" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          <div class="col-md-6 col-md-offset-3">
            <h1>Links</h1>
            <table class="table text-left">
              <tr>
                <th>URL original</th>
                <th>URL shortened</th>
                <th>Visits</th>
              </tr>
              @foreach ($shorteners as $shortener)
                <tr>
                  <td><a href="{{$shortener->shor_original}}">{{$shortener->shor_original}}</a></td>
                  <td><a href="{{URL::to('/').'/s/'.$shortener->shor_short}}">{{URL::to('/').'/s/'.$shortener->shor_short}}</a></td>
                  <td>{{$shortener->shor_visits}}</td>
                </tr>
              @endforeach
            </table>
          </div>
        </form>
      </div>
    </div>      

  </section>
  

  @endsection