  @extends('layouts.main')

  @section('content')

  <section class="container margin-50px-top">

    <div class="row">
      <div class="col-md-12 text-center">  
        <form id="formurl" role="form" method="POST" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          <div class="col-md-6 col-md-offset-3">
            <h1>URL Shortener</h1>
            <div class="form-group">
              <input type="text" class="form-control" name="shor_original" placeholder="Original URL" required>
            </div>
            <div class="width-100 text-right">
              <button type="submit" class="btn">Send</button>
            </div>
            <div class="ajax-success" id="ajax-success">
              <p>Your new URL is:</p>
              <p><a target="_BLANK" id="new-url" href=""></a></p>
            </div>
          </div>
        </form>
      </div>
    </div>      

  </section>
  

  @endsection


  @section('js')
  <script type="text/javascript">
    $("#formurl").submit(function(e) {
      e.preventDefault(); 
      var form = $(this);
      var url = '{{asset('shortener') }}';
      console.log(form.serialize());
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data)
        {
          $("#new-url").attr("href", data)
          $('#new-url').text(data)
          $('#ajax-success').show();
        },error:function(){ 
          alert("error!!!!");
        }
      });
    });
  </script>
  @endsection