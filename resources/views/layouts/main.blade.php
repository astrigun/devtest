<!DOCTYPE html>
<html>
<head>
  <title>Model Management Developer Test</title>
  <meta name="viewport" content="width = 1050, user-scalable = no" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
   <meta name="author" content="Felix Sandoval">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


  <link rel="stylesheet" href="{{asset('css/default.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">

</head>
<body>

  <section>
    <div class="row text-center margin-50px-top">
      <div class="col-md-3 col-md-offset-3">
        <p><a class="btn btn-primary" href="{{ asset('/') }}">HOME</a></p>        
      </div>
      <div class="col-md-3">
        <p><a class="btn btn-primary" href="{{ asset('/links') }}">LINKS</a></p>
      </div>
    </div>
  </section>

  @yield('content')      

  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

  @yield('js')   

</body>



</html>
