<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shorteners extends Model
{
    protected $guarded = [
		'id'
	];
}
