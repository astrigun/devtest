<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use \Storage;
use Response;
use DB;
use Auth;
use Session;
use DateTime;
use App\Shorteners;

class TestController extends Controller
{


    public function shortener(Request $request)
    {

        // This create the new URL and check in DB if exists 
        do {
            $new_url = substr(md5($request->shor_original.mt_rand()),0,8);
            $shortener = Shorteners::where('shor_id', $new_url)->first();
        } while ($shortener != NULL);

        $request->merge([ 'shor_short' => $new_url]);

        $server = App::make('url')->to('/');

        $shorteners = Shorteners::create(request()->except(['_token']));
        return response()->json($server.'/s/'.$shorteners->shor_short);

    }

    public function redirect($url)
    {

        $shortener = Shorteners::where([
            ['shor_short', $url]
        ])
        ->first();

        if ($shortener != null){
            Shorteners::where('shor_id', $shortener->shor_id)->increment('shor_visits');
            return redirect($shortener->shor_original);
        } 
        else return redirect()->back();

    }

    public function links()
    {

        $shorteners = Shorteners::get();

        return view('links', compact('shorteners'));

    }    

}
