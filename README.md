**Model Management Developer Test**

---

## Software

Framework: Laravel 5.8.22

Requirements:
- PHP >= 7.1.3
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Composer

---

## Installation

1. Clone the project
2. Make a "composer install"
3. Create a database called "devtest" 
4. To create a new migration use the make:migration Artisan command: "php artisan migrate" (or import the "devtest.sql", located in database folder)

---

## URLs

- The URL shortener is in the public root folder. Ex: "http://localhost/devtest/public/"
- The links page is in the page "/links". Ex: "http://localhost/devtest/public/links"

---

## Comments

For the URL shortening I used a mt_rand/md5/substr function (that can stores 4.294.967.295 shorten URLs but it is not optimal). With more time, I would have used a Key Generation Service.

---